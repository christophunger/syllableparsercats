val scala3Version = "3.0.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "syllableParserCats",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    //libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-parse" % "0.3.4",
      "org.scalameta" %% "munit" % "0.7.29" % Test
        //  "org.scalameta" %% "munit" % "0.7.29"
    )

  )

testFrameworks += new TestFramework("munit.Framework")


