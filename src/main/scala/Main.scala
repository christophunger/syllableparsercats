/* 
   Copyright 2021 Christoph Johannes Unger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

import SilbenParser.Phonemes.*
import scala.util.matching.Regex
import SilbenParser.*
import Syllable.*
import cats.data.NonEmptyList


@main def explore: Unit = 
  // println(aRhyme)
  // println(anOnset)
  // println(aSyllable)
  // println(parsedSyllable)
  println(parsedResult2)
  println(parsedResult3)
  // println(parsedResult4)
  // println(sonoritaetAllgemein)
  parsedResult5.map(w => println(w))
  // println(msg)

def msg = "Compiling with Scala 3. :)"

// val aRhyme = Rhyme.Branching(Vowels('i'),Coda(Consonants('l')))
// val anOnset = Onset(Consonants('d'))
// val aSyllable = Silbe(Onset(Consonants('d')),Rhyme.Branching(Vowels('i'),Coda(Consonants('l'))))
// //val parsedSyllable = (SilbenParser.consp ~ SilbenParser.consp.peek).map(_._1).parse("lk")
// val parsedSyllable = SilbenParser.wordp.parse("kirin")

// This parsing function recursively inserts an /i/
// at the position where a partial parse fails.
// It presupposes that the first syllable gets parsed sucessfully.
// (First syllables are handled in the parser. )
// It gives satisfactory results in interesting examples,
// but may not insert /i/ at all places where it ought be. 
def parsing(in: String): Either[cats.parse.Parser.Error, (String, Word)] = {
  val initialparse = SilbenParser.wordp.parse(in)
  initialparse match {
    case Right(a,b) if a.isEmpty => Right(a,b)
    case Right(a,b) if consonantsAll(a.head) =>
      val (first, last) = in.splitAt(in.length - a.length)
      parsing(first ++ "i" ++ last)
    case Right(a,b) =>
      val (first, last) = in.splitAt(in.length - a.length)
      parsing(last)
    case Left(l) => Left(l)
  }}

val parsedResult2 = SilbenParser.wordp.parse("blndahî")
val parsedResult3 = spellOutParseResult(parsing("blndahî"))
val parsedResult4 = spellOutParseResult(parsing("hlbjartn"))


val checking = "krn".head.toString ++ "i" ++ "krn".tail
// val sonoritaet = Fricatives('f') match {
//   case a: Fricativet =>
//     val sv = a.sonority
//     s"$a with sonority value $sv"
//   case _ => "Wrong sonority"
// }

val sonoritaetAllgemein = Nasals('m').sonority

// word 'nerm' or 'germ'
val aLongCoda = LongCoda(Fricatives('s'),Plosives('t')) 


val listOfTestingWords = List("b","barnabas","barnabasî","berhemê","berhngarî","berhngarîya","bernedeyn","berprsîyarî","berprsîyarîya","bêhnfrehî","bhîstbû","bnîyat","bnîyatekê","damezrandn","dehmenpîsî","demjmêreka","dernakevît","desthelata","destnîşankrn","destnîşankrî","destpêka","destpêkrn","dewletserê","dfnblnd","dlnermî","drewan","drewîn","drêj","drusttr","dujmnatî","dujmnê","dûŕuyatî","dûŕuyatîya","ferhengokê","germ","grêdayî","guncay","hevŕkî","hjmartn","hjmêrît","htd","hşîyar","hşîyarî","krîyarên","lîstra","medħ","neʔazrînîn","payeberz","payeberzan","payeberzîya","pesendkrî","petros","petrosî","prsîyarek","pşkdarîyê","rastdar","rastdarî","rastdarkrn","romayî","rwî","saxlem","sernekeftme","serxoşî","serperştkar","sunetnebûn","sunetnebûnê","sunetnekrî","wergrtîyye","wernegrtîye","xesandban","xzmeta","xzmetkarê","yûnanî","yûnanîyekî","yûħennay","zkŕeş","zkŕeşî","şahdeyê","ʔafrndeyekê","ʔayînê","ʔaxftbam","ʔencamê","ʔesmanî","ʔesmanîye","ʔêkser","ʔîbrahîmê","ʔîbrahîmî","ʔîbrahîmîne","ʔurşelîma","ʔurşelîmê")

val otherTestList = List("blnd","daxftn","raghandn","hlbjartn","xwîn")

val difficultList = List("xwîngermtr")

val needingCorrection = List("berhngarî","berhngarîya","bêhnfrehî","bhîstbû","damezrandn","demjmêreka","destpêkrn","dfnblnd","dlnermî","drusttr","hevŕkî","pesendkrî","rastdarkrn","sernekeftme","serperştkar","sunetnekrî","xesandban","ʔaxftbam")

// Pretty printing parse results

def spellOutParseResult(pr: Either[cats.parse.Parser.Error,
  (String, Word)]): String = pr match  {
  case Right((a,b)) => b match {
    case Word(c) => spellOutSilbe(c.head) ++
      c.tail.map(s => spellOutSilbe(s)).fold("")(_ ++ _)
    case null => "Null matched in spellOutParseResult 2nd Level"
  }
  case _ => "Parse Error input to spellOutParseResult"}

def spellOutSilbe(syllable: Syllable): String = syllable match {
  case Silbe(Onset(a),Rhyme.Branching(b,ShortCoda(c))) =>
    spellOutConsonants(a).toString ++
    spellOutVowels(b).toString ++
    spellOutConsonants(c).toString
  case Silbe(Onset(a),Rhyme.Branching(b,LongCoda(c,d))) =>
    spellOutConsonants(a).toString ++
    spellOutVowels(b).toString ++
    spellOutConsonants(c).toString ++
    spellOutConsonants(d).toString
  case Silbe(Onset(a),Rhyme.Nonbranching(b)) =>
    spellOutConsonants(a).toString ++
    spellOutVowels(b).toString
  case _ => "Not a wellformed Syllable in spellOutSilbe"
}

def spellOutConsonants(cs: Consonants): Char = cs match {
  case Plosives(p) => p
  case Fricatives(f) => f
  case Liquids(l) => l
  case Nasals(n) => n
  case Semivowels(sv) => sv
  case Aspirant(a) => a
}

def spellOutVowels(vs: Vowels): Char = vs match {
  case Vowels(vs) => vs }

  
val parsedResult5 = needingCorrection.map(i => spellOutParseResult(parsing(i)))
