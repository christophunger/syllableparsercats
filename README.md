## Syllable Parser

This project is for experimentation with the cats.parse library 
of parser combinators for Scala. 
The task is to parse Kurdish (Behdînî) syllables. 

To get an idea of the problems involved, 
consider first the two most common types of syllables: 
open syllables (OS) and closed syllables (CS). 
Open syllables consist of a consonant and a vowel, 
and closed syllables of a consonant-vowel-consonant sequence. 
Doubly closed syllables (consonant-vowel-consonant-consonant) 
and heavy onset syllables (where the onset may consist of 
a very restricted set of consonant-consonant sequences) 
are ignored for the time being. 

The challenge for building a syllable parser 
with parser combinators is 
that not every wrong parsing step in a multisyllabic word 
leads to a parsing failure to trigger backtracking. 
The word *kirin* 'to do' should be parsed as follows: 
*ki.rin* OS-CS. 
But the phoneme sequence *kir* satisfies the criteria 
for a closed syllable, 
so if the parser tries to parse a closed syllable first, 
the parse succeeds, 
but the rest of the word, the sequence *in*, 
can't be parsed, because a syllable must begin with a consonant. 
That is, 
the parser should somehow know 
how to get out of situations like these. 

The solution presented in this first version of the program 
is to divide the syllable into smaller structures 
that are at most binary branching. 
It is possible to combine two parsers 
so that the second one is only used to 'peek ahead', 
not consuming any input. 
This can then be used to test 
e.g. whether a consonant is followed by another consonant. 
This is not the case for *r* in *kirin*, 
therefore it must be an onset. 
Moreover, *n* occurs at the end of the word 
and must therefore be a coda, 
it can not be an onset (because there is no vowel to follow). 

See the comments in the files phoneme.scala and SilbenParser.scala 
for more detailed explanation. 

The second version covers open, closed and doubly closed syllables. 
It does not yet deal with the exceptional onsets 'xw', 'st' and 'str'.

The third version covers 'st', 'sp', 'şk' and 'str' onsets 
as well as 'xw'. 
This latter sequence of letters is interpreted as representing 
one phoneme, a rounded velar fricative, 
which occurs only before a vowel. 

### Usage

`test` runs munit tests. (So far, 37 tests are implemented.)

`run` runs the main function in Main.scala, 
where one can play with other words (or non-words; 
there is no dictionary lookup. 
So one could try parsing *bablaba*, for instance). 

### Abbreviations

The following abbreviations are used in test case descriptions:

OS 
: Open syllable: consonant-vowel 

CS 
: Closed syllable: consonant-vowel-consonant

DCS 
: Double-closed syllable: consonant-vowel-consonant-consonant

ExtraS 
: Pre-onset 's': 's'-consonant-...

ExtraSt 
: Pre-onset 'st': 'st' in *stran* 'song' 


### Next steps

- Testing can always be improved.
