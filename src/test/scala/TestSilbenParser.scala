package catsTest

import SilbenParser.*
import cats.parse.*
import cats.data.*
import Syllable.*
import Rhyme.*
import munit.Clue.generate

class TestSilbenParserSuite extends munit.FunSuite {

  test("1-syllable word /çû/ 'he went' OS") {
    val obtained = SilbenParser.wordp.parseAll("çû").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Fricatives('ç')),Rhyme.Nonbranching(Vowels('û'))),
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word /dil/ 'heart' CS") {
    val obtained = SilbenParser.wordp.parseAll("dil").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Rhyme.Branching(Vowels('i'),ShortCoda(Liquids('l')))),
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /belku/ 'maybe' CS-OS") {
    val obtained = SilbenParser.wordp.parseAll("belku").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('b')),Rhyme.Branching(Vowels('e'),ShortCoda(Liquids('l')))),
      List(Silbe(Onset(Plosives('k')),Rhyme.Nonbranching(Vowels('u'))))))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /kirin/ 'to do' OS-CS") {
    val obtained = SilbenParser.wordp.parseAll("kirin").toString
    val expected = Right(
          Word(NonEmptyList(
              Silbe(Onset(Plosives('k')),Rhyme.Nonbranching(Vowels('i'))),
              List(Silbe(Onset(Liquids('r')),Rhyme.Branching(Vowels('i'),ShortCoda(Nasals('n')))))
          )
          )).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /bingeh/ 'foundation' CS-CS") {
    val obtained = SilbenParser.wordp.parseAll("bingeh").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('b')),Rhyme.Branching(Vowels('i'),ShortCoda(Nasals('n')))),
      List(Silbe(Onset(Plosives('g')),Rhyme.Branching(Vowels('e'),ShortCoda(Aspirant('h')))))
    ))).toString
    assertEquals(obtained, expected)
  }

  test("3-syllable word /bînahî/ 'the sight' in oblique case, OS-OS-OS") {
    val obtained = SilbenParser.wordp.parseAll("bînahî").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('b')),Rhyme.Nonbranching(Vowels('î'))),
      List(Silbe(Onset(Nasals('n')),Rhyme.Nonbranching(Vowels('a'))),
        Silbe(Onset(Aspirant('h')),Rhyme.Nonbranching(Vowels('î')))))
    )).toString
    assertEquals(obtained, expected)
  }

  test("3-syllable word /bingehî/ 'foundation' in oblique case, CS-OS-OS") {
    val obtained = SilbenParser.wordp.parseAll("bingehî").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('b')),Rhyme.Branching(Vowels('i'),ShortCoda(Nasals('n')))),
      List(Silbe(Onset(Plosives('g')),Rhyme.Nonbranching(Vowels('e'))),
        Silbe(Onset(Aspirant('h')),Rhyme.Nonbranching(Vowels('î'))))))).toString
    assertEquals(obtained, expected)
  }

  test("3-syllable word /dilnermî/ 'gentleness', CS-CS-OS") {
    val obtained = SilbenParser.wordp.parseAll("dilnermî").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Rhyme.Branching(Vowels('i'),ShortCoda(Liquids('l')))),
      List(Silbe(Onset(Nasals('n')),Rhyme.Branching(Vowels('e'),ShortCoda(Liquids('r')))),
        Silbe(Onset(Nasals('m')),Rhyme.Nonbranching(Vowels('î')))))
    )).toString
    assertEquals(obtained, expected)
  }

  test("3-syllable word /nêçîrvan/ 'hunter', OS-CS-CS") {
    val obtained = SilbenParser.wordp.parseAll("nêçîrvan").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Nasals('n')),Rhyme.Nonbranching(Vowels('ê'))),
      List(Silbe(Onset(Fricatives('ç')),Rhyme.Branching(Vowels('î'),ShortCoda(Liquids('r')))),           Silbe(Onset(Fricatives('v')),Rhyme.Branching(Vowels('a'),ShortCoda(Nasals('n')))))
    ))).toString
    assertEquals(obtained, expected)
  }

  test("3-syllable word /dergehvan/ 'doorkeeper', CS-CS-CS") {
    val obtained = SilbenParser.wordp.parseAll("dergehvan").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Rhyme.Branching(Vowels('e'),ShortCoda(Liquids('r')))),
      List(Silbe(Onset(Plosives('g')),Rhyme.Branching(Vowels('e'),ShortCoda(Aspirant('h')))),
        Silbe(Onset(Fricatives('v')),Rhyme.Branching(Vowels('a'),ShortCoda(Nasals('n')))))
    ))).toString
    assertEquals(obtained, expected)
  }


  test("4-syllable word /dadkirina/ 'the conviction of x' CS-OS-OS-OS") {
    val obtained = SilbenParser.wordp.parseAll("dadkirina").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Rhyme.Branching(Vowels('a'),ShortCoda(Plosives('d')))),
      List(Silbe(Onset(Plosives('k')),Rhyme.Nonbranching(Vowels('i'))),
           Silbe(Onset(Liquids('r')),Rhyme.Nonbranching(Vowels('i'))),
           Silbe(Onset(Nasals('n')),Rhyme.Nonbranching(Vowels('a'))))))).toString
    assertEquals(obtained, expected)
  }

  test("Sonority values get transmitted") {
    val son = Fricatives('f').sonority
    val obtained = s"Sonority $son"
    val expected = "Sonority 20"
    assertEquals(obtained, expected)

  }

  test("1-syllable word /germ/ 'warm, hot' DCS") {
    val obtained = SilbenParser.wordp.parseAll("germ").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('g')),Rhyme.Branching(Vowels('e'),LongCoda(Liquids('r'),Nasals('m')))),List()))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /guvend/ 'dance' OC-DCS") {
    val obtained = SilbenParser.wordp.parseAll("guvend").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('g')),Rhyme.Nonbranching(Vowels('u'))),
      List(Silbe(Onset(Fricatives('v')),Rhyme.Branching(Vowels('e'),LongCoda(Nasals('n'),Plosives('d')))))))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /hijmart/ 'he counted' CS-DCS") {
    val obtained = SilbenParser.wordp.parseAll("hijmart").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Aspirant('h')),Rhyme.Branching(Vowels('i'),ShortCoda(Fricatives('j')))),
      List(Silbe(Onset(Nasals('m')),Rhyme.Branching(Vowels('a'),LongCoda(Liquids('r'),Plosives('t')))))))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /rastgo/ 'honest' DCS-OS") {
    val obtained = SilbenParser.wordp.parseAll("rastgo").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Liquids('r')),Rhyme.Branching(Vowels('a'),LongCoda(Fricatives('s'),Plosives('t')))),
      List(Silbe(Onset(Plosives('g')),Rhyme.Nonbranching(Vowels('o'))))))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /pirsyar/ 'question' DCS-CS") {
    val obtained = SilbenParser.wordp.parseAll("pirsyar").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('p')),Rhyme.Branching(Vowels('i'),LongCoda(Liquids('r'),Fricatives('s')))),
      List(Silbe(Onset(Semivowels('y')),Rhyme.Branching(Vowels('a'),ShortCoda(Liquids('r')))))))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word /piştrast/ 'upright' DCS-DCS") {
    val obtained = SilbenParser.wordp.parseAll("piştrast").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('p')),Rhyme.Branching(Vowels('i'),LongCoda(Fricatives('ş'),Plosives('t')))),
      List(Silbe(Onset(Liquids('r')),Rhyme.Branching(Vowels('a'),LongCoda(Fricatives('s'),Plosives('t')))))))).toString
    assertEquals(obtained, expected)
  }

  test("3-syllable word /peristgeh/ 'temple' OS-DCS-CS") {
    val obtained = SilbenParser.wordp.parseAll("peristgeh").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('p')),Rhyme.Nonbranching(Vowels('e'))),
      List(Silbe(Onset(Liquids('r')),Rhyme.Branching(Vowels('i'),LongCoda(Fricatives('s'),Plosives('t')))),
        Silbe(Onset(Plosives('g')),Rhyme.Branching(Vowels('e'),ShortCoda(Aspirant('h')))))))).toString
    assertEquals(obtained, expected)
  }

  test("3-syllable word /behnfireh/ 'relaxed' DCS-OS-CS") {
    val obtained = SilbenParser.wordp.parseAll("behnfireh").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('b')),Rhyme.Branching(Vowels('e'),LongCoda(Aspirant('h'),Nasals('n')))),
      List(Silbe(Onset(Fricatives('f')),Rhyme.Nonbranching(Vowels('i'))),
        Silbe(Onset(Liquids('r')),Rhyme.Branching(Vowels('e'),ShortCoda(Aspirant('h')))))))).toString
    assertEquals(obtained, expected)
  }

  test("4-syllable word /desthilatî/ 'power' DCS-OS-CS-OS") {
    val obtained = SilbenParser.wordp.parseAll("desthilatî").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Rhyme.Branching(Vowels('e'),LongCoda(Fricatives('s'),Plosives('t')))),
      List(Silbe(Onset(Aspirant('h')),Rhyme.Nonbranching(Vowels('i'))),
        Silbe(Onset(Liquids('l')),Rhyme.Nonbranching(Vowels('a'))),
        Silbe(Onset(Plosives('t')),Rhyme.Nonbranching(Vowels('î'))))))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with additional 's' before onset /stand/ 'he took, he received' ExtraS-DCS") {
    val obtained = SilbenParser.wordp.parseAll("stand").toString
    val expected = Right(Word(NonEmptyList(
      ExtraS(Fricatives('s')),
      List(Silbe(Onset(Plosives('t')),Branching(Vowels('a'),LongCoda(Nasals('n'),Plosives('d')))))))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with initial 's' as onset /sund/ 'oath', DCS") {
    val obtained = SilbenParser.wordp.parseAll("sund").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Fricatives('s')),Branching(Vowels('u'),LongCoda(Nasals('n'),Plosives('d')))),
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with additional 's' before onset /stêr/ 'star', ExtraS-CS") {
    val obtained = SilbenParser.wordp.parseAll("stêr").toString
    val expected = Right(Word(NonEmptyList(
      ExtraS(Fricatives('s')),
      List(Silbe(Onset(Plosives('t')),Branching(Vowels('ê'),ShortCoda(Liquids('r')))))))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with initial 's' as onset /sar/ 'cold', CS") {
    val obtained = SilbenParser.wordp.parseAll("sar").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Fricatives('s')),Branching(Vowels('a'),ShortCoda(Liquids('r')))), 
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with additional 's' before onset /spî/ 'white', ExtraS-OS") {
    val obtained = SilbenParser.wordp.parseAll("spî").toString
    val expected = Right(Word(NonEmptyList(
      ExtraS(Fricatives('s')),
      List(Silbe(Onset(Plosives('p')),Nonbranching(Vowels('î'))))))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with initial 's' as onset /se/ 'dog', OS") {
    var obtained = SilbenParser.wordp.parseAll("se").toString
    var expected = Right(Word(NonEmptyList(
      Silbe(Onset(Fricatives('s')),Nonbranching(Vowels('e'))),
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with additional 'ş' before onset /şkand/ 'it is broken', ExtraS-DCS") {
    val obtained = SilbenParser.wordp.parseAll("şkand").toString
    val expected = Right(Word(NonEmptyList(
      ExtraS(Fricatives('ş')),
      List(Silbe(Onset(Plosives('k')),Branching(Vowels('a'),LongCoda(Nasals('n'),Plosives('d')))))))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with additional 'st' before onset /stran/ 'song', ExtraSt-DCS") {
    val obtained = SilbenParser.wordp.parseAll("stran").toString
    val expected = Right(Word(NonEmptyList(
      ExtraSt(Fricatives('s'),Plosives('t')),
      List(Silbe(Onset(Liquids('r')),Branching(Vowels('a'),ShortCoda(Nasals('n')))))))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with rounded velar fricative in onset /xwar/ 'he ate' in simple past, CS") {
    val obtained = SilbenParser.wordp.parseAll("xwar").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(RoundVelarFric('x')),Branching(Vowels('a'),ShortCoda(Liquids('r')))),
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("2-syllable word with rounded velar fricative in onset of 2nd syllable /dixwar/ 'he ate' in imperfective past, OS-CS") {
    val obtained = SilbenParser.wordp.parseAll("dixwar").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Nonbranching(Vowels('i'))),
      List(Silbe(Onset(RoundVelarFric('x')),Branching(Vowels('a'),ShortCoda(Liquids('r')))))))).toString
    assertEquals(obtained, expected)
  }

  test("1-syllable word with unrounded velar fricative on onset /xêr/ 'grace', CS") {
    val obtained = SilbenParser.wordp.parseAll("xêr").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Fricatives('x')),Branching(Vowels('ê'),ShortCoda(Liquids('r')))),
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("A rounded velar fricative must be followed by a vowel: /xwîn/ 'blood' works.") {
    val obtained = SilbenParser.wordp.parseAll("xwîn").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(RoundVelarFric('x')),Branching(Vowels('î'),ShortCoda(Nasals('n')))),
      List()))).toString
    assertEquals(obtained, expected)
  }

  test("A rounded velar fricative must be followed by a vowel: */daxw/, non-existing word, does not work.") {
    val obtained = SilbenParser.wordp.parseAll("daxw") match
      case Left(_) => "No success"
      case _ => "Success" 
    val expected = "No success"
    assertEquals(obtained, expected)
  }

  test("A round velar fricative must be followed by a vowel: */doxwtor/, non-existing word, does not work.") {
    val obtained = SilbenParser.wordp.parseAll("daxwtor") match
      case Left(_) => "No success"
      case _ => "Success" 
    val expected = "No success"
    assertEquals(obtained, expected)
  }

  test("An unrounded velar fricative need not be followed by a vowel: /doxtor/ 'physician', works.") {
    val obtained = SilbenParser.wordp.parseAll("doxtor").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Branching(Vowels('o'),ShortCoda(Fricatives('x')))),
      List(Silbe(Onset(Plosives('t')),Branching(Vowels('o'),ShortCoda(Liquids('r')))))))).toString
    assertEquals(obtained, expected)
  }

  test("A rounded velar fricative must be followed by a vowel: /daxwaz/ 'request' works.") {
    val obtained = SilbenParser.wordp.parseAll("daxwaz").toString
    val expected = Right(Word(NonEmptyList(
      Silbe(Onset(Plosives('d')),Nonbranching(Vowels('a'))),
      List(Silbe(Onset(RoundVelarFric('x')),Branching(Vowels('a'),ShortCoda(Fricatives('z')))))))).toString
    assertEquals(obtained, expected)
  }

}
