/* 
   Copyright 2021 Christoph Johannes Unger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package SilbenParser

import Phonemes.*
import cats.parse.*
import Syllable.*
import cats.syntax.validated

object SilbenParser {
  // A parser of vowels
  val vowelp: Parser[Vowels] = Parser
    .charWhere(v => allVowels(v))
    .map(v => Vowels(v))

  // Parsers of consonant subtypes

  // What is written as "xw" is actually one phoneme, a rounded velar fricative. 
  // A rounded velar fricative must be followed by a vowel,
  // e.g. 'xwar' "ate" or 'xwîn' "blood", but not 'daxw'
  val roundvelarfricp = 
    ((Parser.charWhere(Set('x')(_)).soft <* Parser.charWhere(Set('w')(_))
      .map(x => RoundVelarFric(x))) ~ Parser.charWhere(allVowels(_)).peek)
      .map { case (x,_) => RoundVelarFric(x) }
    
  val plosivep: Parser[Plosives] = Parser
    .charWhere(p => plosives(p))
    .map(p => Plosives(p))

  val plosiveVoicedp = Parser
    .charWhere(p => plosivesVoiced(p))
    .map(p => Plosives(p))

  val fricativep: Parser[Fricatives] = Parser
    .charWhere(p => fricatives(p))
    .map(p => Fricatives(p))

  val fricativeVoicedp = Parser
    .charWhere(p => fricativesVoiced(p))
    .map(p => Fricatives(p))

  val nasalp: Parser[Nasals] = Parser
    .charWhere(p => nasals(p))
    .map(p => Nasals(p))

  val liquidp: Parser[Liquids] = Parser
    .charWhere(p => liquids(p))
    .map(p => Liquids(p))

  val semivowelp: Parser[Semivowels] = Parser
    .charWhere(p => semivowels(p))
    .map(p => Semivowels(p))

  val consCharp = Parser.oneOf(
    Parser.charWhere(plosives(_)) ::
      Parser.charWhere(fricatives(_)) ::
      Parser.charWhere(liquidNasal(_)) ::
      Parser.charWhere(aspirant(_)) :: Nil
  )

  val lbNotLongCodap =
    (consCharp.soft ~
      Parser.charWhere(c => allVowels(c)) ~
      Parser.charWhere(c => liquidNasal(c)) ~
      Parser.charWhere(c => labialCsVoiced(c)).peek).mapFilter {
      case (((a,b),c),d) if plosives(a) => Some(Syllable.Silbe(Onset(Plosives(a)),Rhyme.Branching(Vowels(b),ShortCoda(Liquids(c)))))
      case (((a,b),c),d) if fricatives(a) => Some(Syllable.Silbe(Onset(Fricatives(a)),Rhyme.Branching(Vowels(b),ShortCoda(Liquids(c)))))
      case (((a,b),c),d) if liquidNasal(a) => Some(Syllable.Silbe(Onset(Liquids(a)),Rhyme.Branching(Vowels(b),ShortCoda(Liquids(c)))))
      case (((a,b),c),d) if aspirant(a) => Some(Syllable.Silbe(Onset(Aspirant(a)),Rhyme.Branching(Vowels(b),ShortCoda(Liquids(c)))))
      case null => None
    }.backtrack

  val bjNotOneSylp =
    ( Parser.charWhere(c => plosivesVoiced(c)).soft ~
      Parser.charWhere(c => fricativesVoiced(c)).peek ).mapFilter {
      case (a,b) => Some(Syllable.Silbe(Onset(Plosives(a)),Rhyme.Nonbranching(Vowels('i'))))
      case _ => None }.backtrack

  // val highFrontVp = Parser
  //   .charWhere(v => highVowels(v) && frontVowels(v))
  //   .map(v => Vowels(v))

  // val highBackVp = Parser
  //   .charWhere(v => highVowels(v) && backVowels(v))
  //   .map(v => Vowels(v))

  // val nonHighVp = Parser
  //   .charWhere(v => !highVowels(v))
  //   .map(v => Vowels(v))

  // val semiVowelYp: Parser[Semivowels] =
  //   (nonHighVp.peek.with1 ~ highFrontVp ~ (Parser.end orElse consp).peek)
  //     .mapFilter {
  //       case ((a,b),c) => Some(Semivowels('y'))
  //       case null => None }

  // val semiVowelUp: Parser[Semivowels] =
  //   (nonHighVp.peek.with1 ~ highBackVp ~ (Parser.end orElse consp).peek)
  //     .mapFilter {
  //       case ((a,b),c) => Some(Semivowels('w'))
  //       case null => None }

  // val semivowelTestp = Parser.oneOf(
  //   semivowelp :: semiVowelYp :: semiVowelUp :: Nil )



  val aspirantp: Parser[Aspirant] = Parser
    .charWhere(p => aspirant(p))
    .map(p => Aspirant(p))

  // A parser of voiced consonants
  val consVoicedp = Parser.oneOf(
    plosiveVoicedp :: fricativeVoicedp :: nasalp :: liquidp :: semivowelp :: Nil )

  // A parser of consonants
  val consp = Parser.oneOf(
    roundvelarfricp :: plosivep :: fricativep :: nasalp :: liquidp :: semivowelp :: aspirantp :: Nil )


  // Here we start with parsing syllable structure.

  // Special extrametrical Syllable 's' eg. 'stêr' 'star'
  // first we find a word initial 's'
  val initialSp = (Parser.start.with1 ~ Parser.charWhere(s => Set('s','ş')(s)).map(Fricatives(_))) ~ 
                  Parser.charWhere(t => plosivesVoiceless(t)).peek
  // then we find a 't' following a word initial 's' only if followed by 'r' ('stran')
  val initialStp = (initialSp.soft ~ Parser.charWhere(t => Set('t')(t)).map(Plosives(_))) ~
     Parser.charWhere(r => Set('r')(r)).peek
  
  val specialSp: Parser[Syllable] =
    initialSp
      .mapFilter {
        case ((a,b),c) => Some(Syllable.ExtraS(b))
        case null => None }
      .backtrack

  // Special syllable beginning str in 'stran'
  val specialStp: Parser[Syllable] = 
    initialStp
      .mapFilter {
        case ((((a,b),c),d), e) => Some(Syllable.ExtraSt(b,d))
        case null => None }
      .backtrack

  // Onset: always followed by a vowel
  val onsetp: Parser[Onset] =
    (consp.soft ~ vowelp.peek).map((a,b) => Onset(a))

  /* 
   *  Does not work, but doesn't hurt, it does NO work.
   *  But why? Consider "raghandn": 
   *  The first syllable's rhyme may be branching with a short coda. 
   *  This is the first possibility that gets checked, and it is OK.
   *  So "rag" are consumed in the parsing process, 
   *  and it cannot be undone. 
   * 
   */
  val voicedAndAspirantIinsertp =
    (vowelp.peek.with1 ~ (consp.soft ~ Parser.charWhere(c => aspirant(c)).peek)).mapFilter {
      case (a,(b,c))  => Some(Syllable.Silbe(Onset(b), Rhyme.Nonbranching(Vowels('i'))))
      case null => None }.backtrack

  val voicedPlosiveVoicedFricp =
    (plosiveVoicedp.soft ~ fricativeVoicedp.peek).mapFilter {
      case (a,b) => Some(Syllable.Silbe(Onset(a), Rhyme.Nonbranching(Vowels('i'))))
      case null => None }.backtrack

  // This solves "blnd = bilind" and doesn't break existing tests
  val consConsp =
    ((liquidp.backtrack orElse nasalp).soft ~ (liquidp.backtrack orElse nasalp).peek).mapFilter {
      case (a,b) => Some(Syllable.Silbe(Onset(a), Rhyme.Nonbranching(Vowels('i'))))
      case null => None }.backtrack

  

  // A consonant followed by another consonant
  // or
  // a consonant at the end of the word
  // is a short coda consituent.
  // Using .peek to look ahead and not consume the following element.
  val shortcodap: Parser[ShortCoda] =
    (consp.soft ~ consp.peek).map((a,b) => ShortCoda(a)) orElse
    (consp.soft ~ Parser.end.peek).map((a,b) => ShortCoda(a))

  // parsing two consecutive consonants
  val twoconsp = consp.soft ~ consp

  // A long coda has two consonants at the end,
  // where the first one has a higher sonority value than the other
  val longcodap =
    (twoconsp.soft ~ consp.peek).mapFilter {
      case ((a,b),c) if a.sonority > b.sonority => Some(LongCoda(a,b))
      case _ => None } orElse
    (twoconsp.soft ~ Parser.end.peek).mapFilter {
      case ((a,b),c) if a.sonority > b.sonority => Some(LongCoda(a,b))
      case _ => None }

  // Codas may be branching (LongCoda) or non-branching (ShortCoda)
  //  val codap = longcodap.backtrack orElse shortcodap
  val codap = Parser.oneOf(longcodap.backtrack :: shortcodap :: Nil)

  // Rhymes may be nonbranching or branching.
  // A non-branching rhyme consists of the nucleus, which is a vowel
  val nonbranchingp: Parser[Rhyme] = 
    for 
      n <- vowelp
    yield Rhyme.Nonbranching(n)

  // A branching rhyme consists of the nucleus (vowel) and a coda.
  // The coda could be short or long, see above.
  val branchingp: Parser[Rhyme] = 
    for 
      n <- vowelp
      c <- codap
    yield Rhyme.Branching(n, c)

  val branchingShortp =
    for
      n <- vowelp
      c <- shortcodap
    yield Rhyme.Branching(n, c) 

  val branchingLongp =
    for
      n <- vowelp
      c <- longcodap
    yield Rhyme.Branching(n, c)

  // A syllable always consists of an onset and a rhyme.
  val regularsilbenp: Parser[Silbe] = 
    for 
      o <- onsetp
      r <- branchingp.backtrack orElse nonbranchingp
//      r <- Parser.oneOf(branchingLongp.backtrack :: branchingShortp.backtrack :: nonbranchingp :: Nil)
    yield Silbe(o,r)

  // Special syllables when /i/ is not written

  // two consonants word initially

  val firstSylWithoutIp =
    (Parser.start.with1.soft ~ consp ~ consp.peek).mapFilter {
      case ((a,b),c) => Some(Syllable.Silbe(Onset(b),Rhyme.Nonbranching(Vowels('i'))))
      case null => None }.backtrack

  // Three consonants in a row, the second and third could be a Coda

  val threeConsp =
    (consp ~ consp ~ consp ~ Parser.end.peek).mapFilter {
      case (((a,b),c),d) => Some(Syllable.Silbe(Onset(a), Rhyme.Branching(Vowels('i'), LongCoda(b, c))))
      case null => None }.backtrack

  // Left over sequences of two consonants, after regularsilbenp

  val leftOverTwoConsVp =
    (consp ~ consp.peek ~ vowelp.peek).mapFilter {
      case ((a,b),c) => Some(Syllable.Silbe(Onset(a),Rhyme.Nonbranching(Vowels('i'))))
      case null => None }.backtrack

  val leftOverTwoConsEndp =
    (consp ~ consp ~ Parser.end.peek).mapFilter {
      case ((a,b),c) => Some(Syllable.Silbe(Onset(a),Rhyme.Branching(Vowels('i'), ShortCoda(b))))
      case null => None }.backtrack

  val leftOverTwoConsCp = 
    (consp ~ consp.peek ~ consp.peek).mapFilter {
      case ((a,b),c) => Some(Syllable.Silbe(Onset(a),Rhyme.Nonbranching(Vowels('i'))))
      case null => None }.backtrack

  val leftOverTwoConsCcp =
    (consp.peek.with1 ~ consp ~ consp).mapFilter {
      case ((a,b),c) if b.sonority <= c.sonority => Some(Syllable.Silbe(Onset(b),Rhyme.Branching(Vowels('i'), ShortCoda(c))))
      case _ => None }.backtrack

  val shortOneSylWords =
    (Parser.start.peek.with1 ~ consp ~ Parser.end.peek).mapFilter {
      case ((a,b),c) => Some(Syllable.Silbe(Onset(b),Rhyme.Nonbranching(Vowels('i'))))
      case null => None
    }.backtrack

  // ...except when it is /krn/ 'kirin'
  // But in order to check this, I would have to define a type Vib(r)

  val threeConsInRowp =
    (consp.soft ~ consp ~ regularsilbenp.peek).mapFilter {
      case ((a,b),c) => Some(Syllable.Silbe(Onset(a),Rhyme.Branching(Vowels('i'),ShortCoda(b))))
      case null => None }.backtrack

  val twoConspInRowp =
    (consp.soft ~ regularsilbenp.peek).mapFilter {
      case (a,b) => Some(Syllable.Silbe(Onset(a),Rhyme.Nonbranching(Vowels('i'))))
      case null => None }.backtrack

  val comparativeSuffixp =
    (Parser.charWhere(_ == 't') ~ Parser.charWhere(_ == 'r') ~ Parser.end.peek).mapFilter {
      case ((a,b),c) => Some(Syllable.Silbe(Onset(Plosives(a)),Rhyme.Branching(Vowels('i'),ShortCoda(Liquids(b)))))
      case null => None }.backtrack


  // val silbenp = Parser.oneOf(specialStp :: specialSp ::  firstSylWithoutIp :: consConsp :: bjNotOneSylp :: lbNotLongCodap :: regularsilbenp :: Nil)
  val silbenp = Parser.oneOf(shortOneSylWords :: specialStp :: specialSp :: firstSylWithoutIp :: consConsp :: regularsilbenp :: threeConsInRowp :: twoConspInRowp :: comparativeSuffixp :: Nil)

  // A word is a list of syllables.
  val wordp: Parser[Word] = 
    for 
      s <- silbenp.rep
    yield Word(s)

}
