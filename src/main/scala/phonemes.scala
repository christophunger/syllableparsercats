/* 
   Copyright 2021 Christoph Johannes Unger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package SilbenParser

import scala.util.matching.Regex
import cats.data._

object Phonemes {
  // Consonants
  // Plosives
  val plosivesVoiced = Set('b','d','g')
  val plosivesVoiceless = Set('p','t','k','q','\u0294')
  val plosives = plosivesVoiceless ++ plosivesVoiced

  // Fricatives
  val fricativesVoiceless = Set('f','s','ş','x','\u0127', 'ç') // with affricates
  val fricativesVoiced = Set('v','z','j','\u0295', 'c') // with affricates
  val fricatives = fricativesVoiceless ++ fricativesVoiced

  // Nasals, Liquids, Approximants, Semivowels
  val nasals = Set('m','n')
  val liquids = Set('l','ĺ','r','ŕ') // 'h' approximant?
  val liquidNasal = liquids ++ nasals
  val semivowels = Set('y','w') // 'h' semivowel?
  val aspirant = Set('h') // 'h' does not fit in nicely with other categories

  // more consonant subtypes, if needed
  val consonantsVoiceless = plosivesVoiceless ++ fricativesVoiceless
  val consonantsAll = plosives ++ fricatives ++ nasals ++ liquids ++ aspirant // ++ semivowels
  val consonantsNoSemi = plosives ++ fricatives ++ nasals ++ liquids
  val consonantsVoiced = consonantsAll filter (x => !(consonantsVoiceless contains x))
  val consonantsNoPlosives = fricatives ++ nasals ++ liquids // ++ semivowels
  val consonantsApproximants = nasals ++ liquids // ++ semivowels

  // labio-dental consonants
  val labialCs = Set('b','p','m','f','v')
  val labialCsVoiced = Set('b','m','v')
  // coronal consonants
  val coronalCs = Set('t','d','s','z','n','l','r','ŕ')
  val coronalCsVoiced = Set('d','z','n','l','r','ŕ')

  // Vowels
  val highVowels = Set('î','ü','i','û','u')
  val midVowels = Set('ê','e','o')
  val lowVowels = Set('a')

  val frontVowels = Set('î','ü','ê')
  val backVowels = Set('û','u','o')
  val centralVowels = Set('i','e','a')

  val allVowels = highVowels ++ midVowels ++ lowVowels

  val shortVowels = Set('i','u','e')
  val longVowels = allVowels filter (x => !(shortVowels contains x))
  val neutralVowel = Set('i')

  val licensingVowels = allVowels excl 'i'

  val roundedVowels = Set('ü','û','u','o')
  val unroundedVowels = allVowels filter (x => !(roundedVowels contains x))
  val unroundedFrontVowels = frontVowels filter (x => !(roundedVowels contains x))

  val vowelsToDisambiguate = Set('î','û')

  // punctuation; in case we need it.
  val punct = Set('.',',','?','!',';',':','"','«','»','‹','›')

}

import Phonemes.*

trait Phoneme:
   val sonority: Int

trait Plosivet:
   val sonority = 10

trait Fricativet:
   val sonority = 20

trait Nasalt:
   val sonority = 30

trait Approximantt:
   val sonority = 40

// liquid = lateral approximants and vibrants
trait Liquidt:
   val sonority = 50

trait Semivowelt: 
   val sonority = 60

trait Aspirantt: 
   val sonority = 60

trait Voicedt
trait Voicelesst

trait Voweltt:
    val sonority = 100

case class Vowels(v: Char) extends Phoneme with Voweltt {
  require(allVowels(v))
}

case class HighVowels(hv: Char) extends Phoneme with Voweltt {
  require(highVowels(hv))
}

case class MidVowels(mv: Char) extends Phoneme with Voweltt {
  require(midVowels(mv))
}

case class LowVowels(lv: Char) extends Phoneme with Voweltt {
  require(lowVowels(lv))
}

case class FrontVowels(fv: Char) extends Phoneme with Voweltt {
  require(frontVowels(fv))
}

case class BackVowels(bv: Char) extends Phoneme with Voweltt {
  require(backVowels(bv))
}

case class CentralVowels(cv: Char) extends Phoneme with Voweltt {
  require(centralVowels(cv))
}

case class ShortVowels(sv: Char) extends Phoneme with Voweltt {
  require(shortVowels(sv))
}

case class LongVowels(lv: Char) extends Phoneme with Voweltt {
  require(longVowels(lv))
}

case class NeutralVowel(nv: Char) extends Phoneme  with Voweltt {
  require(neutralVowel(nv))
}


trait Consonants

case class Plosives(c: Char) extends Phoneme with Consonants with Plosivet {
  require(plosives(c))
}

case class PlosivesVoiced(c: Char) extends Phoneme with Consonants with Plosivet with Voicedt {
  require(plosivesVoiced(c))
}

case class PlosivesVoiceless(c: Char) extends Phoneme with Consonants with Plosivet with Voicelesst {
  require(plosivesVoiceless(c))
}

case class Fricatives(c: Char) extends Phoneme with Consonants with Fricativet {
  require(fricatives(c))
}

case class FricativesVoiced(c: Char) extends Phoneme with Consonants with Fricativet with Voicedt {
  require(fricativesVoiced(c))
}

case class FricativesVoiceless(c: Char) extends Phoneme with Consonants with Fricativet with Voicelesst {
  require(fricativesVoiceless(c))
}

// What is written as "xw" is actually one phoneme,
// a rounded velar fricative, and can occur in an onset.
case class RoundVelarFric(c: Char) extends Phoneme with Consonants with Fricativet with Voicelesst {
  require(fricativesVoiceless(c))
  override def toString: String = "xw"
}


case class Nasals(c: Char) extends Phoneme with Consonants with Nasalt with Voicedt {
  require(nasals(c))
}

case class Liquids(c: Char) extends Phoneme with Consonants with Liquidt with Voicedt {
  require(liquids(c))
}

case class Semivowels(c: Char) extends Phoneme with Consonants with Semivowelt {
  require(semivowels(c))
}

case class Aspirant(c: Char) extends Phoneme with Consonants with Aspirantt {
  require(aspirant(c))
}


/* Syllables
 * 
 * Syllables in Kurdish consist of an Onset followed by a Rhyme.
 * A Rhyme may be branching or nonbranching. 
 * Nonbranching rhymes consist only of a nucleus, which is a vowel.
 * Branching rhymes consist of a nucleus (vowel) 
 * followed by a Coda. 
 * A coda consists typically of one consonant which is either followed by another consant
 * (which is the onset of the next syllable), 
 * or the end of the word.  
 * 
 * But codas could be branching or non-branching.
 * I implement this by distinguishing LongCoda from ShortCoda. 
 */

// Every syllable starts with an onset.
// The onset is generally a single consonant.
// There are some well-defined exceptions,
// which are ignored for the time being.
case class Onset(o: Consonants)

// Syllable may optionally close with a coda,
// which may contain one or two consonants.
// Here I am using trait Coda and case classes
// so that I can put a precondition in the case class
// for the long coda. (I couldn't do this in an enum.)
trait Coda
case class ShortCoda(c: Consonants) extends Coda
case class LongCoda(
  pc: Consonants & Phoneme,
  mc: Consonants & Phoneme) extends Coda:
   require(pc.sonority > mc.sonority, "LongCoda sonority contour violated")

// Branching and non-branching rhymes as ADT with enum.
enum Rhyme:
  case Branching(n: Vowels, c: Coda)
  case Nonbranching(n: Vowels)

// Tying the necessary elements of syllables together.
enum Syllable:
  case Silbe(o: Onset, r: Rhyme)
  case ExtraS(s: Fricatives)
  case ExtraSt(s: Fricatives, t: Plosives)

// Words are Lists of syllables
import Syllable.*
case class Word(s: NonEmptyList[Syllable])
